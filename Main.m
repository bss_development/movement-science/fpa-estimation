%% This is the main file that runs on IMU data in csv format to obtain 
% foot progression angle.

clear all; close all; clc

% Run the settings file.
settingsFile;

% Allocate space for storing the data.
dataStruct = struct();

% Loop over the subject folders.
for i = 3:length(subjectFolders)
    subjDir = dir([subjectFolders(i).folder '\' subjectFolders(i).name]);
    subjDirFlag = [subjDir.isdir];
    trialFiles = subjDir(~subjDirFlag);

    % Create structure for current subject.
    dataStruct.(subjectFolders(i).name) = struct();
    
    dataStruct.(subjectFolders(i).name).calibration = getCalibration( ...
        join([subjectFolders(i).folder '\' subjectFolders(i).name '\' calibrationFolder],''), fCutOff);

    for j = 1:length(trialFiles)
        currentPath = [trialFiles(j).folder '\' trialFiles(j).name];

        trialName = strrep(['trial_' extractBefore(trialFiles(j).name, '.csv')], ' ','_');

        % Store the data in a useable format.
        dataStruct.(subjectFolders(i).name).(trialName) = readDotCsv(currentPath);

        % calculate all values for each step.
        dataStruct.(subjectFolders(i).name) = ...
            math.calculateSteps(dataStruct.(subjectFolders(i).name), trialName, fCutOff);

    end
end

% Store the data in a results mat file.
save(join([fileFolder '\Results.mat'],''), 'dataStruct')