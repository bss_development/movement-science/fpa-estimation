%% This function reads in data from Movella DOT csv files of the calibration folder.
% filePath - full path to where calibration files are stored.
function dataStruct = getCalibration(filePath, fCutOff)
    
    % Only select the files that are relevant.
    calibDir = dir(filePath);
    calibDirFlag = [calibDir.isdir];
    calibFiles = calibDir(~calibDirFlag);

    % Pre-allocate the data structure.
    dataStruct = struct();

    % Loop over all the files in the calibration folder, such that results
    % could be compared.
    for i = 1:length(calibFiles)

        % Get the current file path and trial name.
        currFilePath = [calibFiles(i).folder '\' calibFiles(i).name];
        trialName = [extractBefore(calibFiles(i).name, '.csv')];

        % Load in the actual Movella Dot CSV file.
        dataStruct.(trialName) = readDotCsv(currFilePath);

        % Pre-allocate a field for the calibration information.
        if i ==1
            dataStruct.(trialName).calibration = struct();    
        end

        % cut-off frequency of the filter.
        dataStruct.(trialName).fCutOff = fCutOff;

        % Determine the calibration orientation.
        dataStruct.(trialName) = math.initOrien(dataStruct.(trialName));       

    end
end