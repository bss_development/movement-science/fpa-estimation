# Foot progression angle estimation

Uses csv files from Movella DOT exporter to calculate foot progression angles assuming that you provide one calibration trial, see for details also the [publication](https://doi.org/10.1186/s12984-021-00816-4).

The calibration trial should be performed with close to 0 degrees of FPA.

The example files include:
- calibration
- toe-in walking
- toe-out walking

Be aware that `settingsFile.m` refers to the data path to use. When specified just run `main.m` to obtain a structure with the processed files. 