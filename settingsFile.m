%% This file contains all the required settings.

% Specify the main direction for the subject data files.
fileFolder = "C:\Measurements\20230912 - FPA";

% Specify where the calibration folder is, has to be placed inside the 
% subject folder.
calibrationFolder = "calibration";

% Extract all folders in that directory to know where to loop over.
allFiles = dir(fileFolder);
dirFlags = [allFiles.isdir];

% Create a structure containing all folders.
subjectFolders = allFiles(dirFlags);

% gravity constant is defined.
gravity = 9.81;

% filter cut off frequency.
fCutOff = 10;