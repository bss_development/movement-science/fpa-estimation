function PlotBars(InputStruct,ylimits,labels)

grid on
grid minor
hold on;

yd = [InputStruct.Optical.Mean InputStruct.Inertial.Mean]';

bar1 = bar([InputStruct.Optical.Mean InputStruct.Inertial.Mean]);

errbar = [InputStruct.Optical.Std InputStruct.Inertial.Std]';
hold on;
pause(1)

for i = 1:2
    errorbar(bar1(i).XData + bar1(i).XOffset, yd(i,:),  errbar(i,:), 'ok', 'LineWidth',2)
end

ylim(ylimits)
xticks(1:5)

set(gca, 'XTickLabel', labels);
set(gca,'FontSize',15)
legend('Optical','Inertial','FontSize',10)
xlabel('Subjects')
ylabel('Angle (degrees)')
end