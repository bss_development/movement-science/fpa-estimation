%% Function to determine when the foot is stationary on the floor.
% inputs:
% acc - Nx3 matrix with acceleration values (in m/s^2).
% gyr - Nx# matrix with gyroscope values (in deg/s).

% outputs:
% tStart - indexes of initial contacts.
% tEnd - indexes of toe-off.
% contactVector - vector with indicators of contact.

function data = zvt(data)

% Obtain the magnitudes of acceleration and angular velocity.
mGyr=sqrt(sum(data.filtGyr.^2,2));
mAcc=sqrt(sum(data.filtAcc.^2,2));

% Initialize N, k and v.
data.N = length(mGyr);
data.contactVector = ones(length(mGyr),1);
varianceWindow = zeros(length(mGyr),1);

% loop over all samples starting at 11 for the first window.
for i=11:data.N
    % Take a snapshot of the acceleration data and average it.
    mAccWindow=mAcc(i-10:i-1); 
    avgmAccWindow=sum(mAccWindow)/10;

    % Calculate the variance within that window.
    varianceWindow(i-10)=sqrt(sum((mAccWindow-avgmAccWindow).^2)/10);

    % If the variance is small, the gyroscope signal is small and the
    % acceleration magnitude is close to gravity the foot should be
    % stationary. For detailed explanation of the conditions see the
    % publication: https://doi.org/10.1186/s12984-021-00816-4
    if varianceWindow(i-10)<0.5 && mGyr(i-10)<50 && (9<mAcc(i-10) && mAcc(i-10)<11)
        data.contactVector(i-10)=0;
    end
end

% Differentiate the contact vector to find indices of the contacts and
% double check that the duration of contact is sufficient (10 samples).
x=diff(data.contactVector);
dx=find(abs(x)==1);

% Correct the contact windows shorter than 100 ms to non-contact.
for i=1:length(dx)-1
    if (dx(i+1)-dx(i))/data.fs <= 0.1
        data.contactVector(dx(i):dx(i+1))=data.contactVector(dx(i)-1);
    end
end

% Recalculate the moments of contact to get indices.
dx=(diff(data.contactVector));
data.tStart=find(dx==1);
data.tEnd=find(dx==-1);
end

