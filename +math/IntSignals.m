function data = intSignals(data, calibration, index)

% Pre-allocate structures for rotated signals.
if index == 1
    data.posLocal = nan(data.N,3);
    data.velLocal = nan(data.N,3);
    data.accLocal = nan(data.N,3);
end
    
% start with initial orientation of unit rotation matrix.
unitRotMat=eye(3);

% Current part of analysis.
indexes = data.tMiddle(index):data.tMiddle(index+1);

% Rotate the gyroscope data to Foot-frame.
gyroLocal = (calibration.rotMatrix*data.filtGyr(indexes,:)')';

% Integrate the signals from this orientation, such that everything is 
% mapped in that frame.
rotMatHat = math.intOmega(unitRotMat,gyroLocal,data.fs);

accLocal = (calibration.rotMatrix*data.filtAcc(indexes,:)')';

% Pre-allocate space.
accFreeLocal = nan(length(indexes),3);

% Gravity.
g = calibration.accOffsetR;

%% Get the free acceleration.
for i = indexes
    accFreeLocal(i-indexes(1)+1,:) = permute(rotMatHat(i-indexes(1)+1,:,:),[2 3 1])*...
        accLocal(i-indexes(1)+1,:)'-g';
end

%% Integrate the free acceleration to obtain velocity.
velLocal = nan(length(indexes),3);
velLocal(1,:) = zeros(1,3);
for i = 2:length(indexes)
    velLocal(i,:) = velLocal(i-1,:) + accFreeLocal(i,:)./data.fs;
end

timeAxis=(0:length(velLocal)-1)/data.fs;
%% Linear velocity drift compensation.
for i = 2:length(indexes)
    velLocal(i,:) = velLocal(i,:) - timeAxis(i)/timeAxis(end) * velLocal(end,:);
end

%% Strapdown integration: obtain position.
posLocal = nan(length(indexes),3);
posLocal(1,:) = zeros(1,3);
for i = 2:length(indexes)
    posLocal(i,:) = posLocal(i-1,:) + velLocal(i,:)./data.fs;
end

data.FPA(index) = atand(posLocal(end,2)/posLocal(end,1));
data.posLocal(indexes,:) = posLocal;
data.velLocal(indexes,:) = velLocal;
data.accLocal(indexes,:) = accFreeLocal;

end