%% This function calculates the orientation of the sensor with respect to
% the walking direction.

% inputs:
% data - structure containing acc, gyr, mag, time, fs fields of data.

% outputs:
% rt - rotation matrix to link sensor orientation to foot orientation.
% rmy - error of the rotation around y axis.
% rmz - error of the rotation around x axis.
% accOffsetR - Offset of the acceleration data.
% tStart - start time of static parts.
% tEnd - end time of static parts.
% contactVector - boolean vector containing indicators of contact.

function data = initOrien(data)

% 2nd order low-pass butterworth filter to get the relevant motion data.
% Implement as filtfilt due to no realtime constraints.
[Bf, Af] = butter(2, (2 * data.fCutOff / data.fs));
data.filtAcc = filtfilt(Bf, Af, data.acc); 
data.filtGyr = filtfilt(Bf, Af, data.gyr);

%% Step detection using the zero velocity update.
data = math.zvt(data);

% Correct the calculated steps.
data = math.correctSteps(data);

% Find the Static and Dynamic parts of the calibration trial.
staticPart = floor(0.2*data.tStart):floor(0.8*data.tStart);
steps = find(diff(data.contactVector));
if data.contactVector(1) == 1
    steps(1) = [];
end
dynamicPart = steps(1):steps(6);

% Determine bias and subtract it.
data.gyroBias = mean(data.gyr(staticPart,:));
gyroNoBias = data.gyr - data.gyroBias;

% Obtain the perpendicular walking direction principal component.
yAxis = pca((gyroNoBias(dynamicPart,:)-mean(gyroNoBias(dynamicPart,:))));

% Normalize that direction to get one component of the calibration matrix.
normYaxis = yAxis(:,1)/norm(yAxis(:,1));

% determination of the (normalized) z-axis, average of 3 heel lifts is taken.
zax = mean(data.acc(staticPart,:));
zaxis = zax/norm(zax); 

% cross product between the rotation axis and z-axis
xas = cross(normYaxis',zaxis)/norm(cross(normYaxis',zaxis));
tzt = cross(xas,normYaxis)/norm(cross(xas,normYaxis));

% Calculate the errors of the axes with respect to being perpendicular.
data.rmz = sqrt((tzt-zaxis).^2);
txt = cross(zaxis,xas)/norm(cross(zaxis,xas));
data.rmy = sqrt((txt-normYaxis').^2);

% the rotation matrix  is obtained by adding the three directions together.
data.rotMatrix = inv([xas',normYaxis,tzt']);

% Determine acc offset (gravity)
accOffset = mean(data.acc(staticPart,:));

% Rotate acc offset to foot frame.
data.accOffsetR = (data.rotMatrix*accOffset')';
end