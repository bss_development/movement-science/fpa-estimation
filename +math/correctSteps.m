%% This function makes sure that the steps start with contact and end with
% a full contact phase.

% input:
% dataStruct - containing tStart, tEnd vectors to correct.

% output:
% dataStruct - with correct start and end indexes.

function dataStruct = correctSteps(dataStruct)

if ~isempty(dataStruct.tStart)  
    % Correct for wrong first step.
    if dataStruct.tEnd(1) < dataStruct.tStart(1)
        dataStruct.tEnd(1) = [];
    end
     
    % End with a full step.
    if dataStruct.tEnd(end) < dataStruct.tStart(end)
        dataStruct.tStart(end) = [];
    end
    
    % Get the middle of contact phases for later calculations.
    dataStruct.tMiddle = floor((dataStruct.tStart(2:end) + ...
        dataStruct.tEnd(1:end-1))/2);
else
    disp("NO steps detected.")
end

end