function rotMat = intOmega(initRotMat, omega, fs)
% intOmega.m - Calculates orientation matrix by integrating gyroscope signal
%
% Inputs: 
% initRotMat        Initial orientation matrix
% omega             Angular velocity (rad/s)
% fs                Sample frequency
%
% Outputs: 
% rotMat               Orientation matrix [Nsamples x 3 x 3]
%
% Martin Schepers, July 2005
% Henk Kortier, Sep 2011
% Frank Wouda, Aug 2023

N = size(omega,1);               
omegaMatrix = zeros(N,3,3);

for i=1:N
    omegaMatrix(i,:,:) = [0 -omega(i,3) omega(i,2); 
                               omega(i,3) 0 -omega(i,1);
                               -omega(i,2) omega(i,1) 0];
end

rotMat = zeros(N,3,3);
rotMat(1,:,:) = initRotMat;  % initial value
        
for i=2:N
    R = permute(rotMat(i-1, :, :),[2 3 1]);
    
    Rdot = R*permute(omegaMatrix(i,:,:),[2 3 1]);   % Integrate R
    R = R + (1/fs).*Rdot;
    
    [A,~]=qr(R);                                 % create proper rotation matrix
    rotMat(i,:,:) = sqrt(A.*A).*sign(R);            % get correct sign
end