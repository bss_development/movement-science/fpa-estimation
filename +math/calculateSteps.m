%% This function calculates the step parameters of interest.

% input:
% dataStruct - containing identified steps and acc/gyro data.

% output:
% dataStruct - containing calculated FPA.

function dataStruct = calculateSteps(dataStruct, trialName, fCutOff)

% Get the trial data.
data = dataStruct.(trialName);

% Get all calibration files.
calibrationFields = fieldnames(dataStruct.calibration);

% By default take the first calibration trial.
calibration = dataStruct.calibration.(calibrationFields{1});

% Store the cut-off frequency.
data.fCutOff = fCutOff;

% 2nd order low-pass butterworth filter to get the relevant motion data.
% Implement as filtfilt due to no realtime constraints.
[Bf, Af] = butter(2, (2 * data.fCutOff / data.fs));
data.filtAcc = filtfilt(Bf, Af, data.acc); 
data.filtGyr = filtfilt(Bf, Af, data.gyr);

% Step detection using the zero velocity update.
data = math.zvt(data);

% Correct the steps.
data = math.correctSteps(data);

% Number of steps is 1 smaller as it should end with foot contact.
steps = 1:(length(data.tMiddle)-1);

% Loop over all steps.    
for iStep = steps

    % Pre-allocate space for the FPA values.
    if iStep == 1
        data.FPA = nan(length(steps),1);
    end

    % Integrate the signals to obtain the FPA within a step.
    data = math.intSignals(data, calibration, iStep);

end

% Store the processed data in the original structure.
dataStruct.(trialName) = data;
end