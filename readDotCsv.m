%% This function reads in data from Movella DOT csv files.

function dataStruct = readDotCsv(filePath)

    % Load in the actual Movella Dot CSV file.
    currentData = readtable(filePath, 'Delimiter', ',');

    % Pre-allocate a structure.
    dataStruct = struct();

    % Store the data in a useable format.
    dataStruct.acc = table2array(currentData(:,{'Acc_X', 'Acc_Y', 'Acc_Z'}));
    dataStruct.gyr = table2array(currentData(:,{'Gyr_X', 'Gyr_Y', 'Gyr_Z'}))*pi/180;
    dataStruct.mag = table2array(currentData(:,{'Mag_X', 'Mag_Y', 'Mag_Z'}));
    dataStruct.time = table2array(currentData(:,'SampleTimeFine')- ...
        currentData(1,'SampleTimeFine'))/1000000;

    % Calculate the magnitic norm and sample frequency
    dataStruct.magNorm = sum(dataStruct.mag.^2, 2);
    dataStruct.fs = round(1/(dataStruct.time(2) - dataStruct.time(1)));

end